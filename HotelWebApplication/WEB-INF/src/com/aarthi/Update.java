package com.aarthi;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UpdateMenu
 */

public class Update extends HttpServlet {


	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Update() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException,NullPointerException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter out=response.getWriter();
	    response.setContentType("text/html; charset=utf-8");

		int id=Integer.parseInt(request.getParameter("id"));
		request.getSession().setAttribute("id",id);
		
		String update=request.getParameter("update");
		request.getSession().setAttribute("update", update);
		
		if(request.getParameter("remove-item")!=null)
			try
			{String query="delete from menu where Food_id=?";
							Class.forName("com.mysql.jdbc.Driver"); 
						     Connection connection= DriverManager.getConnection("jdbc:mysql://localhost:3306/hotel", "root", "aarthi123");
						    PreparedStatement st=connection.prepareStatement(query);
						    
							st.setInt(1,id);
							int res=st.executeUpdate(); 
							
							out.println("Data deleted successfully");
			        }
		

			catch(Exception e)
			{
				out.println("Something went wrong");
			}	
		else
		{
			
			out.println("<!DOCTYPE html>");
			out.println("<head>");
			out.println("<title>Admin</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("Input new value: ");
			out.println("<form method=post action=ModifyItem>");
			out.println("<input type=text name=new_value>");
			out.println("<input type=submit name=Submit value=Submit>");
			out.println("</form>");
			out.println("</body>");
			out.println("</html>");


		}
			}
	


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request,response);
}
}
