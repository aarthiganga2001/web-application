package com.aarthi;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminValidate
 */

public class AdminValidate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdminValidate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		validateAdmin(request,response);
	}
	

	protected void validateAdmin(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		// TODO Auto-generated method stub
		
		response.setContentType("text/html;charset=UTF-8");
		
		String adminName="admin";
		String adminPassword="12345";
		
		try(PrintWriter out=response.getWriter())
		{
	
	
		if(request.getParameter("admin_name").equals(adminName)&&request.getParameter("admin_password").equals(adminPassword))
		{
			out.println("<!DOCTYPE html>");
			out.println("<head>");
			out.println("<title>Admin</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Welcome Admin<h1>");
			out.println("<a href=/HotelWebApplication/add-hotels.jsp><h4>Add hotels</h4></a>");
		        out.println("<a href=/HotelWebApplication/view-hotels.jsp><h4>View hotels</a>");
			out.println("<a href=/HotelWebApplication/update-hotels.html><h4>Update hotels</a>");
			out.println("<a href=/HotelWebApplication/add-menu.html><h4>Add menu</a>");
	                out.println("<a href=/HotelWebApplication/view-menu.html><h4>View menu</a>");
			out.println("<a href=/HotelWebApplication/update-menu.html><h4>Update menu</h4></a>");
			out.println("</body>");
			out.println("</html>");	
		}
		else
		{
			out.println("<!DOCTYPE html>");
			out.println("<head");
			out.println("<title>Admin</title>");
			out.println("</head>");
			out.println("<body>");
			out.println("<h1>Invalid login<h1>");
			out.println("</body>");
			out.println("</html>");	
		}
		
		}
	}

}

