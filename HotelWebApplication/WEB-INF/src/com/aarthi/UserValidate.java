package com.aarthi;

import java.sql.*;
import java.io.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UserValidate
 */
//@WebServlet("/UserValidate")
public class UserValidate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserValidate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();

		try {
		
		int user_id=Integer.parseInt(request.getParameter("user_id"));
		request.getSession().setAttribute("user_id",user_id);
		int flag=0;

		String connectionURL="jdbc:mysql://localhost:3306/hotel";
		Class.forName("com.mysql.jdbc.Driver"); 
		Connection  connection= DriverManager.getConnection(connectionURL, "root", "aarthi123");
	    
		 PreparedStatement st=connection.prepareStatement("select user_name from users where user_id=?");
		 st.setInt(1,user_id);
		 
		 ResultSet res=st.executeQuery();
		 
		 String uname,upassword;
		 while(res.next())
		 {
		 flag=1;
		 uname=res.getString("user_name");
		 out.println("Welcome "+uname);
		 out.println("<html>");
		 out.println("<head>");
		 out.println("<title>user-roles</title>");
		 out.println("</head>");
		 out.println("<body>");
		 out.println("<a href=/HotelWebApplication/view-hotels.jsp><h4>View Hotels</h4></a><br>");
		 out.println("<a href=/HotelWebApplication/view-menu.html><h4>View Menu</h4></a><br>");
		 out.println("<a href=/HotelWebApplication/add-to-menu.html><h4>Add To Cart</h4></a><br>");
		 out.println("<a href=/HotelWebApplication/buy-menu.jsp><h4>Buy Menu</h4></a>");
		 out.println("</body>");
		 out.println("</html>");
		 }
		 
		 if(flag==0)
		 {
                 out.println("Invalid login");
                 }
		}
		catch(Exception e)
		{
			out.println("Something went wrong");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

