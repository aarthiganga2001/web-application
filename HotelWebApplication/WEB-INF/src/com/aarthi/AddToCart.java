
package com.aarthi;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

/**
 * Servlet implementation class Update
 */
//@WebServlet("/AddToCart")
public class AddToCart extends HttpServlet {
	
	String query;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddToCart() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter out=response.getWriter();

		try {
		String value=request.getSession().getAttribute("user_id").toString();
		
		String val=request.getParameter("food_id");
		int fid=Integer.parseInt(val);
		
		
		int iD=Integer.parseInt(value);

		String connectionURL="jdbc:mysql://localhost:3306/hotel";
		Class.forName("com.mysql.jdbc.Driver"); 
		Connection  connection= DriverManager.getConnection(connectionURL, "root", "aarthi123");
		 PreparedStatement st=connection.prepareStatement("insert into cart(user_id,food_id) values(?,?)");
		 st.setInt(1,iD);
		 st.setInt(2,fid);
			
		st.executeUpdate(); 
			
			out.println("Added to cart successfully");
		
	}
		catch(Exception e)
		{
			out.println("Something went wrong");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

