package com.aarthi;

import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddMenu
 */

public class AddMenu extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddMenu() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out=response.getWriter();

		out.println("<!DOCTYPE html>");
		out.println("<head>");
		out.println("<title>Add menu</title>");
		out.println("</head>");
		out.println("</html>");
		
		String query="insert into menu"+"(Food_category,Food_name,Food_price,hotel_id)"+"values(?,?,?,?)";
		try{
			String connectionURL="jdbc:mysql://localhost:3306/hotel";
			Class.forName("com.mysql.jdbc.Driver"); 
			Connection  connection= DriverManager.getConnection(connectionURL, "root", "aarthi123");
		    PreparedStatement st=connection.prepareStatement(query);
		    
		    String foodCategory =request.getParameter("food_category");
		    String foodName =request.getParameter("food_name");
		    String foodPrice =request.getParameter("food_price");
		    int hotel_id=Integer.parseInt(request.getParameter("hotel_id"));

			st.setString(1,foodCategory);
			st.setString(2,foodName);
			st.setString(3,foodPrice);
			st.setInt(4,hotel_id);
			
			int res=st.executeUpdate(); 
			
			out.println("Data added successfully");
			}
			
		catch(Exception e)
		{
			out.println("Invalid hotel id");
		}
	}

}


