package com.aarthi;

import java.sql.*;
import java.io.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UserValidate
 */
//@WebServlet("/UserValidate")
public class UserSetUp extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserSetUp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());

                int id=0;
                
		PrintWriter out=response.getWriter();

		try {
                String uname=request.getParameter("user_name");
                String upassword=request.getParameter("user_password");

		
		String connectionURL="jdbc:mysql://localhost:3306/hotel";
		Class.forName("com.mysql.jdbc.Driver"); 
		Connection  connection= DriverManager.getConnection(connectionURL, "root", "aarthi123");
	   
		 PreparedStatement st=connection.prepareStatement("insert into users(user_name,user_password)values(?,?)");
		 st.setString(1,uname);
		 st.setString(2,upassword);
		 st.executeUpdate();
		 PreparedStatement st2=connection.prepareStatement("select user_id from users where user_name=? and user_password=?");
		 st2.setString(1,uname);
		 st2.setString(2,upassword);
		 ResultSet res=st2.executeQuery();
		 while(res.next())
		 {
		 id=res.getInt("user_id");
		 }
		 out.println("Account SetUp Successful");
		 out.println("User Id:"+id);
              
		}
		catch(Exception e)
		{
			out.println("Account already exists");
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

