package com.aarthi;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.*;

/**
 * Servlet implementation class Update
 */

public class ModifyItem extends HttpServlet {
	
	String query;
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ModifyItem() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		PrintWriter out=response.getWriter();

		try {
		String value=request.getParameter("new_value");
		//out.println(value);
		String val=request.getSession().getAttribute("id").toString();
		//out.println(val);
		String type= request.getSession().getAttribute("update").toString();
		//out.println(type);
		int id=Integer.parseInt(val);
		float price;

		String connectionURL="jdbc:mysql://localhost:3306/hotel";
		Class.forName("com.mysql.jdbc.Driver"); 
		Connection  connection= DriverManager.getConnection(connectionURL, "root", "aarthi123");
	    
		 PreparedStatement st;
		 
		if(type.equalsIgnoreCase("Category"))
		{

			query="update menu set Food_category=? where Food_id=?";
		    st=connection.prepareStatement(query);

			st.setString(1,value);
			st.setInt(2,id);
		}
		else if(type.equalsIgnoreCase("Name"))
		{

			query="update menu set Food_name=? where Food_id=?";
			   st=connection.prepareStatement(query);

			st.setString(1,value);
			st.setInt(2,id);

		}
		else
		{

			query="update menu set Food_price=? where Food_id=?";

		    st=connection.prepareStatement(query);


            price=Float.parseFloat(value);

           // out.println(price);

			st.setFloat(1,price);
			st.setInt(2,id);
			
		}
			
		    
			
			int res=st.executeUpdate(); 
			
			out.println("Data updated successfully");
		
	}
		catch(Exception e)
		{
			out.println("Something went wrong");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

